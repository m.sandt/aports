# Contributor: Patrycja Rosa <alpine@ptrcnull.me>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=py3-pbs-installer
pkgver=2024.8.9
pkgrel=0
pkgdesc="Installer for Python Build Standalone"
url="https://github.com/frostming/pbs-installer"
arch="noarch"
license="MIT"
makedepends="py3-gpep517 py3-pdm-backend"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="https://github.com/frostming/pbs-installer/archive/$pkgver/py3-pbs-installer-$pkgver.tar.gz"
builddir="$srcdir/pbs-installer-$pkgver"
options="!check" # tests/ directory empty

build() {
	export PDM_BUILD_SCM_VERSION="$pkgver"
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
9c8d80fb4630a5dd292acd0ac2a0519800813fa06e8811f4f0fc3911236d72dc407ebb36f5e87d79c1743cb7f824189417acffeaa6f84c4493961fe4d08765a8  py3-pbs-installer-2024.8.9.tar.gz
"
