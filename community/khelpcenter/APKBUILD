# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=khelpcenter
pkgver=24.08.0
pkgrel=0
pkgdesc="Application to show KDE Applications' documentation"
# armhf blocked by extra-cmake-modules
# x86 blocked by reduced qtwebengine featureset
# armv7, ppc64le, s390x, riscv64, loongarch64 blocked by qt6-qtwebengine
arch="all !armhf !x86 !armv7 !ppc64le !s390x !riscv64 !loongarch64"
url="https://userbase.kde.org/KHelpCenter"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	grantlee-dev
	karchive-dev
	kbookmarks-dev
	kconfig-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kservice-dev
	ktexttemplate-dev
	kwindowsystem-dev
	libxml2-dev
	qt6-qtbase-dev
	qt6-qtwebengine-dev
	samurai
	xapian-core-dev
	"
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/system/khelpcenter.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/khelpcenter-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e74aa8a28daeee2fd1140d7cdca2997760b8e7da50f758a87fe9e7d3556f88deeaff1d198afa3366b2a7bca585a278a809b6c65e76b84544858c6cfd9e24066b  khelpcenter-24.08.0.tar.xz
"
