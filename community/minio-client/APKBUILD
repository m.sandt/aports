# Contributor: Chloe Kudryavtsev <code@toast.bunkerlabs.net>
# Contributor: Hoang Nguyen <folliekazetani@protonmail.com>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=minio-client
pkgver=0.20240826.104958
# 0.20230323.200304 -> 2023-03-23T20-03-04Z
_pkgver="${pkgver:2:4}-${pkgver:6:2}-${pkgver:8:2}T${pkgver:11:2}-${pkgver:13:2}-${pkgver:15:2}Z"
pkgrel=0
pkgdesc="The MinIO Client"
url="https://min.io/"
arch="all"
license="AGPL-3.0-or-later"
makedepends="go"
source="https://github.com/minio/mc/archive/RELEASE.$_pkgver/minio-client-$pkgver.tar.gz"
builddir="$srcdir/mc-RELEASE.$_pkgver"

# secfixes:
#   0.20230111.031416-r0:
#     - CVE-2022-41717

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	local prefix='github.com/minio/mc/cmd'
	local date=${_pkgver%%T*}
	local time=${_pkgver#*T}

	go build -tags kqueue -o bin/mcli -ldflags "
		-X $prefix.Version=${date}T${time//-/:}
		-X $prefix.CopyrightYear=${date%%-*}
		-X $prefix.ReleaseTag=RELEASE.$_pkgver
		-X $prefix.CommitID=0000000000000000000000000000000000000000
		-X $prefix.ShortCommitID=000000000000
		"
}

check() {
	# mc/cmd is disabled, seems to be outdated and fails on all my systems
	# shellcheck disable=2046
	go test -tags kqueue $(go list ./... | grep -v cmd)
}

package() {
	install -Dm755 bin/mcli -t "$pkgdir"/usr/bin/
}

sha512sums="
f024ba1e763b32d1c2e9315e607487d7f9dfe77c87bed542d8e67612688c78846b9164cf3fd27e73d80c8685bee6b591825bc15d9509da84b163ae9821d2e13f  minio-client-0.20240826.104958.tar.gz
"
