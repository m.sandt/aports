# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=calendarsupport
pkgver=24.08.0
pkgrel=0
pkgdesc="Library providing calendar support"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt6-qtwebengine -> akonadi
# armv7 blocked by messagelib -> akonadi-calendar
# loongarch64 blocked by pimcommon
arch="all !armhf !ppc64le !s390x !riscv64 !armv7 !loongarch64"
url="https://kontact.kde.org"
license="GPL-2.0-or-later AND Qt-GPL-exception-1.0 AND LGPL-2.0-or-later"
depends_dev="
	akonadi-calendar-dev
	akonadi-dev
	akonadi-mime-dev
	akonadi-notes-dev
	kcalendarcore-dev
	kcalutils-dev
	kcodecs-dev
	kguiaddons-dev
	kholidays-dev
	ki18n-dev
	kidentitymanagement-dev
	kio-dev
	kmime-dev
	pimcommon-dev
	qt6-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	qt6-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
subpackages="$pkgname-dev $pkgname-lang"
_repo_url="https://invent.kde.org/pim/calendarsupport.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/calendarsupport-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
7e4306e91adf4d3427d20ab1511eeca1a970cc2581e11c139dd089f3e3ef85294b524ec2b8f24ba6607f131bfe449ad6c9fad80bb823af023edcdf1518e41e76  calendarsupport-24.08.0.tar.xz
"
