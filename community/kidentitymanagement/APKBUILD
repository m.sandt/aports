# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kidentitymanagement
pkgver=24.08.0
pkgrel=0
pkgdesc="KDE PIM libraries"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kontact.kde.org"
license="LGPL-2.0-or-later"
depends_dev="
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kiconthemes-dev
	kio-dev
	kpimtextedit-dev
	ktextwidgets-dev
	kxmlgui-dev
	qt6-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	kirigami-addons-dev
	samurai
	"
checkdepends="xvfb-run"
subpackages="$pkgname-dev $pkgname-lang"
_repo_url="https://invent.kde.org/pim/kidentitymanagement.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kidentitymanagement-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	# kpimidentity-signaturetest is broken
	xvfb-run ctest --test-dir build --output-on-failure -E "kpimidentity-signaturetest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
23af3062353a896f976fee46875986d30ee518ef8577cfe3837506ec2ef9b8fc06e42abff1880eedfc75477968463ea05ec411975516c37626dc0d22fb90456f  kidentitymanagement-24.08.0.tar.xz
"
