# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=ktimer
pkgver=24.08.0
pkgrel=0
pkgdesc="A little tool to execute programs after some time"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/utilities/org.kde.ktimer"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	knotifications-dev
	kstatusnotifieritem-dev
	kwidgetsaddons-dev
	qt6-qtbase-dev
	samurai
	"
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/utilities/ktimer.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/ktimer-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DKF_IGNORE_PLATFORM_CHECK=ON # does not have metainfo.yaml
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
893872aa333aebc8765d025f2a7736a9e01d2f94be8efa17de2e6738190f4d3cb327bd7e13c28e5dce70b1c95eb566652b18c19309e0c62d18557a3ec614fb19  ktimer-24.08.0.tar.xz
"
