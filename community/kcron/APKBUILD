# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kcron
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/system/"
pkgdesc="Configure and schedule tasks"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kconfigwidgets-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	qt6-qtbase-dev
	samurai
	"
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/system/kcron.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kcron-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
991c23df1c739b00b717ed6a72d28c5b0a554c0da15151ef48d16d87d9cc27b9d30b1ed11b8f0691b913b8ccdf7f87ece8b737577b33a956836c49febc032072  kcron-24.08.0.tar.xz
"
